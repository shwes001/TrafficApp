angular.module('starter', ['ionic','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.form', {
      url: '/form',
      views: {
        'menuContent': {
          templateUrl: 'templates/form.html',
          controller: 'FormCtrl'
        }
      }
    })

    .state('app.list', {
      url: '/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/list.html',
          controller: 'ListCtrl'
        }
      }
    });

  $urlRouterProvider.otherwise('/app/login');
})

.controller('AppCtrl', function($scope,$ionicPopup) {
  $scope.logout = function () {
    var confirmPopup = $ionicPopup.confirm({
     title: 'Logout',
     template: 'Are you sure you want to logout?'
   });

   confirmPopup.then(function(res) {
     if(res) {
        ionic.Platform.exitApp();
     } else {
       console.log('You are not sure');
     }
   });
  }
})

.controller('LoginCtrl', function($scope,$location) {
  $scope.login = function(input) {
    if (input.email === '' || input.password === '') {
      alert('Please enter valid details');
    } else {
      $location.path('/app/form');
    }
  }
})

.controller('FormCtrl', function($scope,$location,$http,$cordovaCamera) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $scope.penaltyType = 'Helmet';
  $scope.amount = '100';
  $scope.selectValue = function(value) {
    if ( value === 'Helmet') {
       $scope.amount = '100';
    } else if ( value === 'Seat Belt') {
       $scope.amount = '500';
    } else if ( value === 'One Way Traffic') {
       $scope.amount = '500';
    } else if ( value === 'Others') {
       $scope.amount = '';
    } else {
       $scope.amount = '';
    }
  }
  $scope.findDetails = function(data) {
    if (data === undefined) {
      console.log('do nothing');
    } else {
      $http.get('js/data.json')
        .then(function(response) {
            $scope.response = response.data;
            $scope.name = $scope.response[0].ownerName;
            $scope.phone = $scope.response[0].ownerPhone;
            $scope.address = $scope.response[0].ownerAddress;
        });
    }
  }
  $scope.setData = function(isChecked) {
    if(isChecked) {
      $scope.driverName = $scope.response[0].ownerName;
      $scope.driverPhone = $scope.response[0].ownerPhone;
      $scope.driverAddress = $scope.response[0].ownerAddress;
    } else {
      $scope.driverName = '';
      $scope.driverPhone = '';
      $scope.driverAddress = '';
    }
  }
  $scope.takePhoto = function () {
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
  };

      $cordovaCamera.getPicture(options).then(function (imageData) {
          $scope.imgURI = "data:image/jpeg;base64," + imageData;
      }, function (err) {
          alert('error occured');
      });
  }
  $scope.submit = function(data,name,phone,address,driverName,driverPhone,driverAddress) {
    if (data.amount === undefined || name === undefined || phone === undefined || address === undefined || driverName === undefined 
      || driverAddress === undefined || driverPhone === undefined) {
      alert('Data not filled');
    } else {
      $location.path('/app/list');
    }
  }
})

.controller('ListCtrl', function($scope,$location,$http) {
  $scope.$on('$ionicView.beforeEnter', function (e,config) {
    config.enableBack = false;
  });
  $http.get('js/penalty.json')
    .then(function(response) {
        $scope.response = response.data;
    });
})

